### Theme for Mr. Baconpants
View site here: www.mrbaconpants.com  
Currently using Foundation 6.0.5.

### Based off JointsWP
JointsWP is a blank WordPress theme built with Foundation 6, giving you all the power and flexibility you need to build complex, mobile friendly websites without having to start from scratch.

Starting its humble life as a fork of the popular theme Bones, JointsWP is now the foundation of thousands of websites across the globe.

### Changes

- None so far...

### Getting Started With Gulp
- Install [node.js](https://nodejs.org).
- Using the command line, navigate to your theme directory
- Run npm install
- Run gulp to confirm everything is working

[Read more about how Gulp is used with JointsWP.](http://jointswp.com/docs/gulp/)